module.exports = {
  outputDir: 'dist',
  chainWebpack(config) {
    config.devtool('source-map')
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/_base.scss";`
      }
    }
  }
}
