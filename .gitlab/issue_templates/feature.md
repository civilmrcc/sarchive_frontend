**Use Case Title**

e.g. I as a researcher must be able to ...

**Primary Actor**

e.g. Researcher, Moonbird crew member, ...

**Precondition**

e.g. I am on the screen where I see the list of cases...

**Missing funcationality**

e.g. User must click here, see that, etc.

**Success Condition**

e.g. Information must be visible and ...

**Additional context**

Add any other context or screenshots about the feature request here.

/label ~feature