# CivilMRCC SARchive

<!-- Generated with Markdown All-in-One (yzhang.markdown-all-in-one) for VSCode -->
- [CivilMRCC SARchive](#civilmrcc-sarchive)
  - [Project Description](#project-description)
    - [Input via OneFleet application:](#input-via-onefleet-application)
    - [Input directly via CivilMRCC SARchive](#input-directly-via-civilmrcc-sarchive)
  - [Main objective](#main-objective)
  - [Technical Components](#technical-components)
    - [Internal structure of the project:](#internal-structure-of-the-project)
  - [Contributing](#contributing)
    - [Issues](#issues)
  - [Developer Guide](#developer-guide)
    - [Development Workflow](#development-workflow)
      - [Project setup](#project-setup)
      - [Development Mode](#development-mode)
      - [Compiles and minifies for production](#compiles-and-minifies-for-production)
      - [Run your unit tests](#run-your-unit-tests)
      - [Lints and fixes files](#lints-and-fixes-files)
    - [Apollo Client](#apollo-client)
    - [Environments](#environments)
  - [HOW-TOs](#how-tos)
    - [Attach to Chrome for debugging](#attach-to-chrome-for-debugging)
    - [Debug Vue.JS control state](#debug-vuejs-control-state)
    - [How to test locally with another server](#how-to-test-locally-with-another-server)
    - [How to generate new types for the GraphQL API?](#how-to-generate-new-types-for-the-graphql-api)
    - [How to deploy to the testing server](#how-to-deploy-to-the-testing-server)
    - [Customize configuration](#customize-configuration)


The corresponding backend project can be found [here](https://gitlab.com/civilmrcc/sarchive_backend).

## Project Description

The CivilMRCC SARchive is the place where data about cases (search and rescue events, distress cases) will get stored. The data input will be possible via the OneFleet application or directly via an interface at the CivilMRCC SARchive.

### Input via OneFleet application:

For open distress case which got put in and coordinated via the OneFleet app there will be a possibility to transfer them at a specific point to the CivilMRCC SARchive. This specific point needs to be discussed and defined, most likely it will be at the moment of a finished rescue.

### Input directly via CivilMRCC SARchive

The CivilMRCC SARchive will provide a way to input data about closed distress cases directly. This would be applicable for cases where there was no direct involvment or coordination of any SAR NGOs.

The data shall be used for internal statistics and research as well as potentially be (partly) opened to researchers, journalistis, ...
Therefore powerful options to search and display the data will be needed.

## Main objective

- Search historic cases
- Filter cases by certain criteria
- Export filtered subsets to other media (e.g. CSV) for further processing

## Technical Components

The application is mainly based on the following components:

![Technical Component Diagram](./img/civilmrcc-sarchive-frontend.png)

- [Vue.js](https://vuejs.org) along with [Vue Class Component](https://class-component.vuejs.org)
- [BootstrapVue](https://bootstrap-vue.org), a vue wrapper for [Bootstrap](https://getbootstrap.com)
- [Vue Apollo](https://apollo.vuejs.org) to connect the backend

Of course, there are more 3rd-party libraries used in this project, e.g. `vue2-leaflet` to make displaying a map more simple. For a complete list, check the `package.json`.

### Internal structure of the project:
- The frontend is entirely API driven. This means that the API defines the data model and the frontend consumes it based on generated types. To update the frontend's API definition, a working backend is required. For more info on the generator, see [Apollo Client](#apollo-client)
- [Environments](#environments) are used to manage the connection to the backend
- Since the project is not (yet) very complex, some simple machansims are used to apply its behavior:
  - a single vue instance (`serverBus`) is used as a global event bus to handle most of the application events
  - a single vue component (`src/components/Common/Modals.vue`) handles the display and hide for all modal dialogs in the application
  - messages du the user are displayed as alerts
- All UI components are defined in `src/components`
- Shared functionality is defined in `src/common`

For more detailed information on how to start, check the [Developer Guide](#developer-guide).

## Contributing

See our [Developer Guide](#developer-guide) and [Code of Conduct](CODE_OF_CONDUCT.md).

### Issues

Please report all issues in the [issue page](https://gitlab.com/niczem/casesdb_frontend/issues)

## Developer Guide

- Code is written in [TypeScript](https://www.typescriptlang.org).
- Tests are written using [jest](https://jestjs.io) and [@vue/test-utils](https://vue-test-utils.vuejs.org)
### Development Workflow

We use Kanban, where the issues on the frontend project (this one) define the next item(s) to work on, while the [backend](https://gitlab.com/civilmrcc/sarchive_backend) needs to follow.

Check this [board](https://gitlab.com/civilmrcc/sarchive_frontend/-/boards) for an overview.

Important cornerstones of the workflow are:

- label issue with `To Do` if you want to raise it's priority and have a developer pick it "next"
- label issue as `Doing` and assign yourself when you are working on it, to avoid two people working on the same
- label issue with `backend` if there is a dependency in the backend project and link the backend issue
- To be transparent about changes and deployment status:
  - work on issues in `feature/*` or `fix/` branches (see [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow))
  - label issue as `done`, when developments/changes are done and merged to the `develop` branch
  - close issues which are `done`, when the respective changes was merged from `develop` to `master`
    - this is done at an aligned point in time via merge request, by a maintainer of the repository/ies
  - The state of `master` will automatically be deployed to testing

#### Project setup

```sh
npm install
```

#### Development Mode

Run app with watch task via

```sh
npm start
```

#### Compiles and minifies for production

```sh
npm run build
```

#### Run your unit tests

```sh
npm run test
```

or for watcher-based re-runs

```sh
npm run test:watch
```

#### Lints and fixes files

```sh
npm run lint
```

### Apollo Client

[Vue Apollo](https://apollo.vuejs.org) comes with a CLI to generate API types as interfaces and enums based on a given GraphQL API endpoint and the queries defined in the application. There is dedicated package script to run that generator.

```sh
npm run codegen
```

There are several places, where the output `*.ts` will end up:
- `src/global-query-types.ts` if they are globally relevant
- in a `graphql` subfolder next to the file defining the `gql` query tag

The latter means, that there are several `graphql` subfolders in the project tree.

The single file handling direct interaction with the apollo client api is located in `src/common/api/graphqlClient.ts`.
### Environments

`.env.*` files in the project root are used to configure the application with respect to the environment it is executed in. These are the supported environments:
- `development` for local development against a backend instance on the same machine
- `production` for production deployment, the backend url is injected via environment variables
- `test` for unit tests

## HOW-TOs

### Attach to Chrome for debugging

- Install the `Debugger for Chrome` extension for VSCode/VSCodium
- Run the app via `npm start`
- Launch Chrome with debugger enabled via `F5` (see `.vscode/launch.json`)
- Debug!

### Debug Vue.JS control state

Use the Firefox (or Chrome) Vue.js devtools plugin: https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/

### How to test locally with another server

Change the url in the `env.development` file.

### How to generate new types for the GraphQL API?

1. Define the `gql` query or mutation in your vue component or a dedicated file
2. Run `npm run codegen`
3. The apollo-client code generator will generate the types in `<folder_of_the_query>/graphql/<query_name>.ts`

### How to deploy to the testing server

Similarly to the process documented in the [onefleet README](https://gitlab.com/civilmrcc/onefleet):

- login to the server via ssh
- clone this repository to your home folder
- export the database url for the backend via `export DATABASE_URL=<url>`, where the url has the format `postgres://user:password@host:port/dbname`
- run `sudo -E ./deploy.sh` to pull the frontend and backend images and run the stack based on the compose file
- to have watchtower pull new images and deploy them, run `sudo -E docker run -d --name watchtower-sarchive -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower sarchive_frontend sarchive_backend`

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
