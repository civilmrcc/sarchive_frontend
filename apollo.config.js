module.exports = {
  client: {
    service: {
      name: 'cases_backend',
      url: process.env.VUE_APP_BACKEND_URL
    },
    includes: ['src/**/*.vue', 'src/**/*.ts']
  }
}
