#!/bin/sh

docker pull registry.gitlab.com/civilmrcc/sarchive_frontend/master:latest
docker pull registry.gitlab.com/civilmrcc/sarchive_backend/master:latest
docker-compose up -d
