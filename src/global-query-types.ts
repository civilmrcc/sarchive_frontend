/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum BoatColor {
  BLACK = "BLACK",
  BLUE = "BLUE",
  BROWN = "BROWN",
  GRAY = "GRAY",
  GREEN = "GREEN",
  RED = "RED",
  WHITE = "WHITE",
  YELLOW = "YELLOW",
}

export enum BoatType {
  FIBERGLASS = "FIBERGLASS",
  OTHER = "OTHER",
  RUBBER = "RUBBER",
  WOOD = "WOOD",
}

export enum CaseOutcome {
  AFM_RESCUE = "AFM_RESCUE",
  AUTONOMOUS_ARRIVAL = "AUTONOMOUS_ARRIVAL",
  EMPTY_BOAT = "EMPTY_BOAT",
  ITALY_RESCUE = "ITALY_RESCUE",
  LYCG_INTERCEPTION = "LYCG_INTERCEPTION",
  MERV_INTERCEPTION = "MERV_INTERCEPTION",
  MERV_RESCUE = "MERV_RESCUE",
  NGO_RESCUE = "NGO_RESCUE",
  RETURNED_INDEPENDENTLY_OR_BY_FISHERMEN = "RETURNED_INDEPENDENTLY_OR_BY_FISHERMEN",
  SHIPWRECK = "SHIPWRECK",
  TN_INTERCEPTION = "TN_INTERCEPTION",
  UNKNOWN_OUTCOME = "UNKNOWN_OUTCOME",
}

export enum CaseStatus {
  CLOSED = "CLOSED",
  ONGOING_RESCUE_INTERCEPTION = "ONGOING_RESCUE_INTERCEPTION",
  OPEN_APPROACHING = "OPEN_APPROACHING",
  OPEN_CONFIRMED_VISUALLY = "OPEN_CONFIRMED_VISUALLY",
  OPEN_KNOWN_POSITION = "OPEN_KNOWN_POSITION",
  OPEN_LOST_CONTACT = "OPEN_LOST_CONTACT",
  OPEN_UNKNOWN = "OPEN_UNKNOWN",
  RESCUE_COMPLETED = "RESCUE_COMPLETED",
}

export enum ErrorCodes {
  AUTH_LOGON_FAILED = "AUTH_LOGON_FAILED",
  AUTH_MISSING_PERMISSION = "AUTH_MISSING_PERMISSION",
  AUTH_NOT_AUTHENTICATED = "AUTH_NOT_AUTHENTICATED",
  AUTH_PASSWORD_NEEDS_CHANGE = "AUTH_PASSWORD_NEEDS_CHANGE",
  AUTH_TOKEN_ERROR_USER = "AUTH_TOKEN_ERROR_USER",
  AUTH_TOKEN_EXPIRED = "AUTH_TOKEN_EXPIRED",
  AUTH_TOKEN_INVALID = "AUTH_TOKEN_INVALID",
  AUTH_TOKEN_MISSING = "AUTH_TOKEN_MISSING",
  AUTH_USER_NOT_ACTIVE = "AUTH_USER_NOT_ACTIVE",
  AUTH_USER_NOT_FOUND = "AUTH_USER_NOT_FOUND",
  AUTH_WRONG_PASSWORD = "AUTH_WRONG_PASSWORD",
  CASE_NOT_FOUND = "CASE_NOT_FOUND",
  INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT",
  TAG_ALREADY_EXISTS = "TAG_ALREADY_EXISTS",
  TAG_NOT_FOUND = "TAG_NOT_FOUND",
  USER_ALREADY_EXISTS = "USER_ALREADY_EXISTS",
  USER_PWD_CANT_BE_SAME_AS_CURRENT = "USER_PWD_CANT_BE_SAME_AS_CURRENT",
}

export enum FrontextInvolvement {
  NO = "NO",
  SUSPECTED = "SUSPECTED",
  YES = "YES",
}

export interface BoatInput {
  type?: BoatType | null;
  color?: BoatColor | null;
  engineStatus?: string | null;
  freetext?: string | null;
}

export interface CreateCaseInput {
  caseNumber: string;
  freetext?: string | null;
  peopleOnBoard?: PeopleOnBoardInput | null;
  boat?: BoatInput | null;
  position?: PositionInput | null;
  links?: (string | null)[] | null;
  tags?: (TagInput | null)[] | null;
  timestamp?: string | null;
  frontexInvolvement?: FrontextInvolvement | null;
  outcome?: CaseOutcome | null;
  status?: CaseStatus | null;
  estimatedDepartureTime?: string | null;
  authoritiesAlerted?: boolean | null;
  authoritiesDetails?: string | null;
}

export interface FilterInput {
  text?: string | null;
  caseDateFrom?: string | null;
  caseDateTo?: string | null;
  boatTypes?: (BoatType | null)[] | null;
  boatColors?: (BoatColor | null)[] | null;
  tags?: (TagInput | null)[] | null;
}

export interface PaginationInput {
  offset?: number | null;
  limit?: number | null;
}

export interface PeopleOnBoardInput {
  women?: number | null;
  men?: number | null;
  minors?: number | null;
  total?: number | null;
  medical?: number | null;
  missing?: number | null;
  drowned?: number | null;
}

export interface PositionInput {
  latitude: number;
  longitude: number;
  timestamp?: string | null;
  speedOverGround?: number | null;
  courseOverGround?: number | null;
}

export interface TagInput {
  name: string;
}

export interface UpdateCaseInput {
  id: string;
  caseNumber?: string | null;
  freetext?: string | null;
  peopleOnBoard?: PeopleOnBoardInput | null;
  boat?: BoatInput | null;
  links?: (string | null)[] | null;
  tags?: (TagInput | null)[] | null;
  timestamp?: string | null;
  frontexInvolvement?: FrontextInvolvement | null;
  outcome?: CaseOutcome | null;
  status?: CaseStatus | null;
  estimatedDepartureTime?: string | null;
  authoritiesAlerted?: boolean | null;
  authoritiesDetails?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
