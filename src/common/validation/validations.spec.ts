import { isInteger, isFloat } from './validations'

describe('validation', () => {
  it('isInteger - 100 true', () => {
    expect(isInteger('100')).toBeTruthy()
  })
  it('isInteger - 100.5 false', () => {
    expect(isInteger('100.5')).toBeFalsy()
  })
  it('isInteger - Abc false', () => {
    expect(isInteger('Abc')).toBeFalsy()
  })
  it('isInteger - 0 true', () => {
    expect(isInteger('0')).toBeTruthy()
  })
  it('isFloat - 100.5 true', () => {
    expect(isFloat('100.5')).toBeTruthy()
  })
  it('isFloat - 100 false', () => {
    expect(isFloat('100')).toBeFalsy()
  })
  it('isFloat - Abc false', () => {
    expect(isFloat('Abc')).toBeFalsy()
  })
})
