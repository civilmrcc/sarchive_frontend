import { extend } from 'vee-validate'
import { required, min, digits, numeric } from 'vee-validate/dist/rules'

export const isInteger = (value: string) => /^-?\d*$/.test(value)
export const isFloat = (value: string) => /^-?\d+\.\d+$/.test(value)

extend('required', { ...required, message: 'This field is required' })
extend('min', { ...min, message: 'Values does not meet minimal length' })
extend('digits', { ...digits, message: 'Minimal number of digits not met' })
extend('integer', {
  validate: value => isInteger(value),
  message: 'Only integer values allowed'
})
extend('float', {
  validate: value => isFloat(value),
  message: "Only numbers and decimal '.' allowed"
})
extend('notmatch', {
  validate: (value, args) => args?.length && value !== args[0],
  message: 'Value is not allowed'
})
extend('match', {
  validate: (value, args) => args?.length && value === args[0],
  message: 'Value does not match'
})
extend('number', {
  validate: (value, args) => isInteger(value) || isFloat(value),
  message: 'Only integers or decimals allowed'
})
