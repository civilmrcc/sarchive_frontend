import client from './api/graphqlClient'
import { storageInstance } from './storage'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import gql from 'graphql-tag'
import { MeQuery } from './graphql/MeQuery'
import { FetchResult } from 'apollo-link'

const ME_QUERY = gql`
  query MeQuery {
    me {
      username
    }
  }
`

const KEY_TOKEN = 'token'

export const getToken = () => {
  return storageInstance.fetch(KEY_TOKEN)
}

export const isAuthenticated = () => {
  const token = getToken()
  return token != null && token.length > 0
}

export const storeToken = (token: string) => {
  storageInstance.save(KEY_TOKEN, token)
}

export const getUsername = async (apollo: DollarApollo<Vue>): Promise<string | null> => {
  if (!apollo) {
    return null
  }
  const result = (await apollo.query<MeQuery>({ query: ME_QUERY })) as FetchResult<MeQuery>
  const data = result.data as MeQuery
  return data.me?.username || null
}

export const logout = async () => {
  await client.clearStore()
  storageInstance.clear()
}
