import { filterGraphQlProperties } from './helpers'

describe('helpers', () => {
  it('filterGraphQlProperties', () => {
    const testObject = {
      __typename: 'Testtype',
      test: '123'
    }
    const result = filterGraphQlProperties(testObject)
    expect(Object.keys(result).length).toEqual(1)
    expect(Object.keys(result).includes('__typename')).toBeFalsy()
  })
})
