enum LogLevel {
  FATAL = 0,
  ERROR = 1,
  WARN = 2,
  INFO = 3,
  DEBUG = 4,
  TRACE = 5
}

const MAX_LOG_LEVEL = LogLevel.INFO

const createMessage = (level: LogLevel, message: string) => {
  if (level > MAX_LOG_LEVEL) {
    return null
  }

  const now = new Date()
  return (
    now.getHours() +
    ':' +
    now
      .getMinutes()
      .toString()
      .padStart(2, '0') +
    ':' +
    now
      .getSeconds()
      .toString()
      .padStart(2, '0') +
    '|' +
    level +
    '|' +
    message
  )
}

const writeToLog = (message: string | null) => {
  if (!message) {
    return
  }
  if (process.env.VUE_APP_DEBUG) {
    console.log(message)
  }
  // TODO Log to backend log API in production
}

const writeToErrorLog = (message: string | null) => {
  if (!message) {
    return
  }
  if (process.env.VUE_APP_DEBUG) {
    console.error(message)
  }
  // TODO Log to backend log API in production
}

/**
 * The single logger API allowing to pass tog messages to the backend at some point in the future.
 */
const logger = {
  fatal: (error: Error) => {
    writeToErrorLog(
      createMessage(LogLevel.FATAL, error.name + '\n' + error.message + '\n' + error.stack)
    )
  },
  error: (error: Error | string) => {
    if (error instanceof Error) {
      writeToErrorLog(
        createMessage(LogLevel.ERROR, error.name + '\n' + error.message + '\n' + error.stack)
      )
    } else {
      writeToLog(createMessage(LogLevel.ERROR, error))
    }
  },
  warn: (message: string) => {
    writeToLog(createMessage(LogLevel.WARN, message))
  },
  info: (message: string) => {
    writeToLog(createMessage(LogLevel.INFO, message))
  },
  debug: (message: string) => {
    writeToLog(createMessage(LogLevel.DEBUG, message))
  },
  trace: (message: string) => {
    writeToLog(createMessage(LogLevel.TRACE, message))
  }
}

export default logger
