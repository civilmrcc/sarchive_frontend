import Vue from 'vue'

/**
 * The single global event bus in the application.
 */
const serverBus = new Vue()

export default serverBus
