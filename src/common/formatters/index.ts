/* istanbul ignore file */

import { formatDegrees, formatLocation as formatLocationInternal } from './GeoLocationFormatter'
import CaseFormatter from './CaseFormatter'

export const formatLatLng = (value: number): string => formatDegrees(value)
export const formatLocation = (value: { latitude: number; longitude: number } | null): string =>
  value ? formatLocationInternal(value) : ''
export const formatFirstUp = (value: string): string =>
  new CaseFormatter().ensureFirstLetterUpper(value)
