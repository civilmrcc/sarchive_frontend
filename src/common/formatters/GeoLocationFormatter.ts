import { LatLon } from '../converters/CoordinateConverter'
import {
  formatCoordinates as formatCoordinatesInternal,
  COORDINATE_FORMATS
} from 'coordinate-formats'
import { FormatOptions } from 'coordinate-formats/lib/types'

const LON_LAT_DIGITS = 6

export const formatDegrees = (value: number): string => {
  return value.toFixed(LON_LAT_DIGITS)
}

export const formatCoordinates = (coordinates: LatLon) => {
  const formattingOptions: FormatOptions = {
    format: COORDINATE_FORMATS.DM,
    useCardinalDirections: false
  }
  return formatCoordinatesInternal(coordinates, formattingOptions)
}

export const formatLocation = (value: { latitude: number; longitude: number }) => {
  return formatCoordinates(value as LatLon)
}
