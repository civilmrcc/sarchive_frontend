import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { onError } from 'apollo-link-error'
import { setContext } from 'apollo-link-context'
import { getToken } from '../authentication'
import logger from '../logger'
import serverBus from '../serverBus'
import { ApolloLink } from 'apollo-link'
import { Events } from '../constants'
import { GraphQLError } from 'graphql'
import { handleBackendError, emitError } from './backendErrorHandler'

let backendUrl: string | undefined

/**
 * Mandatory backend API, except for unit tests.
 */
if (process.env.NODE_ENV !== 'test') {
  backendUrl = process.env.VUE_APP_BACKEND_URL
  if (backendUrl == null || backendUrl.length === 0) {
    throw new Error('Environment variable VUE_APP_BACKEND_URL not provided')
  } else {
    logger.info(`Using backend at '${backendUrl}'`)
  }
}

/**
 * HTTP connection to the API. Must be an absolute URL.
 */
const httpLink = createHttpLink({
  uri: backendUrl
})

/**
 * Centrally handles errors and error codes emitted by the backend.
 * @param error
 */
const handleErrorCode = (error: GraphQLError) => {
  const { message, path } = error

  handleBackendError(message, path)
}

/**
 * Handles all network related errors.
 * @param networkError
 */
const handleNetworkError = networkError => {
  emitError(`[Network error]: ${networkError}`)

  if (networkError.hasOwnProperty('statusCode')) {
    // ServerError is not exported from apollo-link-error, hence statusCode is neither
    const statusCode = networkError['statusCode']
    logger.debug(`Network error status code is ${statusCode}`)
    if (statusCode === 401 || statusCode === 500) {
      serverBus.$emit(Events.loggedOut)
    }
  }

  if (networkError.toString().includes('Failed to fetch')) {
    logger.error('Network connection failed')
    serverBus.$emit(Events.loggedOut)
  }
}

/**
 * apollo-link implementation capturing both server-side and network errors.
 */
const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors?.length) {
    graphQLErrors.forEach(graphqlError => handleErrorCode(graphqlError))
  }

  if (networkError) {
    handleNetworkError(networkError)
  }
})

/**
 * apollo-link implementation handling the authentication mechanism (Bearer token) with the backend.
 */
const authLink = setContext((_, { headers }) => {
  // get the authentication token to enrich the request
  const token = getToken()
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

/**
 * Apollo client in-memory cache
 */
const cache = new InMemoryCache()

/**
 * The apollo-client instance as a composition of all links defined above.
 */
const client = new ApolloClient({
  link: ApolloLink.from([errorLink, authLink, httpLink]),
  cache: cache
})

export default client
