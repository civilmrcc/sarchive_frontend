import { ErrorCodes } from '../../global-query-types'
import { extractErrorCode } from './backendErrorHandler'

describe('backendErrorHandler', () => {
  it('extractErrorCode, extracts Code', () => {
    const result = extractErrorCode(
      `Error: ${ErrorCodes.AUTH_TOKEN_EXPIRED}. This is an error message.`
    )
    expect(result).toEqual(ErrorCodes.AUTH_TOKEN_EXPIRED)
  })
  it('extractErrorCode, returns null when no code is found', () => {
    const result = extractErrorCode(`Error: XXX. This is an error message.`)
    expect(result).toBeNull()
  })
})
