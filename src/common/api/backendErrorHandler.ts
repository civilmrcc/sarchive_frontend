import { ErrorCodes } from '../../global-query-types'
import serverBus from '../serverBus'
import { Events } from '../constants'
import logger from '../logger'

export function emitError(error: string) {
  logger.error(error)
  serverBus.$emit(Events.error, error)
}

export function extractErrorCode(message: string): ErrorCodes | null {
  if (!message) {
    return null
  }
  const errorCodeCandidates = message.match(/([A-Z]|[0-9]|_){5,}/g)
  if (!errorCodeCandidates?.length) {
    return null
  }
  return errorCodeCandidates[0] as ErrorCodes
}

export function handleBackendError(
  message: string,
  path: readonly (string | number)[] | undefined
) {
  const errorCode = extractErrorCode(message)

  switch (errorCode) {
    case ErrorCodes.AUTH_MISSING_PERMISSION:
      emitError('Action is not allowed.')
      break
    case ErrorCodes.AUTH_PASSWORD_NEEDS_CHANGE:
      emitError('You are required to change your password.')
      serverBus.$emit(Events.showPasswordChangeModal)
      break
    case ErrorCodes.AUTH_TOKEN_INVALID:
    case ErrorCodes.AUTH_TOKEN_ERROR_USER:
    case ErrorCodes.AUTH_TOKEN_EXPIRED:
    case ErrorCodes.AUTH_TOKEN_MISSING:
      emitError("You're login session expired. Please log in.")
      serverBus.$emit(Events.loggedOut)
      break
    case ErrorCodes.AUTH_NOT_AUTHENTICATED:
    case ErrorCodes.AUTH_LOGON_FAILED:
    case ErrorCodes.AUTH_WRONG_PASSWORD:
    case ErrorCodes.AUTH_USER_NOT_FOUND:
      emitError('Incorrect user name or password.')
      break
    case ErrorCodes.AUTH_USER_NOT_ACTIVE:
      emitError('User is inactive.')
      break

    // Error codes not relevant to UI
    // case ErrorCodes.TAG_ALREADY_EXISTS:
    // case ErrorCodes.TAG_NOT_FOUND:
    // case ErrorCodes.CASE_NOT_FOUND:
    // case ErrorCodes.USER_ALREADY_EXISTS:
    // case ErrorCodes.USER_PWD_CANT_BE_SAME_AS_CURRENT:

    default:
      emitError(`Backend error '${message}' for API path '${path}'.`)
      break
  }
}
