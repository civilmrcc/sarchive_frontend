import CaseFormInput from '../../models/CaseFormInput'
import { BoatColor, BoatType } from '../../global-query-types'
import { convertFormToCreate, convertCaseToFormInput, determineTotalPob } from './CaseConverter'
import { CasesQuery_cases_cases } from '../../components/Cases/graphql/CasesQuery'

describe('CaseConverters', () => {
  describe('convertCaseFormInput', () => {
    let testInput: CaseFormInput
    beforeEach(() => {
      testInput = {
        boatColor: BoatColor.RED,
        boatType: BoatType.RUBBER,
        creationDate: '2020-01-01',
        creationTime: '12:00'
      } as CaseFormInput
    })

    it('Converts with default values without error', () => {
      convertFormToCreate(testInput)
    })

    it('links not added if null', () => {
      const result = convertFormToCreate(testInput)
      expect(result.case.links).toEqual([])
    })

    it('result valid - multiple links', () => {
      testInput.links = [{ value: 'test1.com' }, { value: 'test2.com' }, { value: 'test3.com' }]
      const result = convertFormToCreate(testInput)
      expect(result.case!.links!.length).toEqual(3)
      expect(result.case!.links![1]).toEqual('test2.com')
    })

    it('result valid - pob', () => {
      testInput.womenPob = 3
      const result = convertFormToCreate(testInput)
      expect(result.case!.peopleOnBoard!.women).toEqual(3)
    })

    it('convertCaseToFormInput', () => {
      const inputCase = {
        timestamp: '2020-01-01 12:12:12',
        freetext: 'this is free text',
        boat: {
          color: BoatColor.RED,
          engineStatus: 'broken',
          type: BoatType.RUBBER
        },
        peopleOnBoard: {
          women: 1,
          medical: 1,
          minors: 1,
          men: 1,
          drowned: 1,
          missing: 1
        },
        tags: [{ name: 'tag1' }]
      } as CasesQuery_cases_cases

      const result = convertCaseToFormInput(inputCase)

      expect(result.creationDate).toEqual('2020-01-01')
      expect(result.creationTime).toEqual('12:12')
      expect(result.freetext).toEqual(inputCase.freetext)
      expect(result.boatColor).toEqual(inputCase.boat?.color)
      expect(result.medicalPob).toEqual(inputCase.peopleOnBoard?.medical)
      expect(result.tags![0]['value']).toEqual('tag1')
    })

    it('determineTotalPob', () => {
      const inputCase = {
        peopleOnBoard: {
          women: 1,
          medical: 1,
          minors: 1,
          men: 1,
          drowned: 1,
          missing: 1,
          total: 0
        }
      } as CasesQuery_cases_cases

      const result = determineTotalPob(inputCase as CasesQuery_cases_cases)
      expect(result).toEqual(3)

      inputCase.peopleOnBoard!.total = 5

      const result2 = determineTotalPob(inputCase as CasesQuery_cases_cases)
      expect(result2).toEqual(5)
    })
  })
})
