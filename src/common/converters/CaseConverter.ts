import CaseFormInput from '../../models/CaseFormInput'
import { CreateCaseMutationVariables } from '../../components/EditCase/graphql/CreateCaseMutation'
import {
  CasesQuery_cases_cases,
  CasesQuery_cases_cases_tags
} from '../../components/Cases/graphql/CasesQuery'
import {
  BoatColor,
  BoatType,
  CaseOutcome,
  CaseStatus,
  FrontextInvolvement
} from '../../global-query-types'
import logger from '../logger'
import {
  combineDateTime,
  dateToBackendString,
  backendStringToInputDateString,
  backendStringToInputTimeString
} from './DateTimeConverter'
import { UpdateCaseMutationVariables } from '../../components/EditCase/graphql/UpdateCaseMutation'

const toBoatType = (boatType: string) => {
  return BoatType[boatType]
}

const toBoatColor = (boatColor: string) => {
  return BoatColor[boatColor]
}

const toInt = (num: number | undefined) => {
  return num ? Math.round(num) : 0
}

/**
 * Converts contents of the case form to the data structure required by the CREATE api
 * @param caseFormInput
 */
export const convertFormToCreate = (caseFormInput: CaseFormInput): CreateCaseMutationVariables => {
  const timestamp = dateToBackendString(
    combineDateTime(caseFormInput.creationDate!, caseFormInput.creationTime!)
  )

  const departureTime =
    caseFormInput.estimatedDepartureDate && caseFormInput.estimatedDepartureTime
      ? dateToBackendString(
          combineDateTime(
            caseFormInput.estimatedDepartureDate!,
            caseFormInput.estimatedDepartureTime!
          )
        )
      : null

  const caseObject: CreateCaseMutationVariables = {
    case: {
      caseNumber: caseFormInput.caseNumber!,
      timestamp: timestamp,
      boat: {
        type: toBoatType(caseFormInput.boatType!),
        color: toBoatColor(caseFormInput.boatColor!),
        engineStatus: caseFormInput.engineStatus ?? ''
      },
      peopleOnBoard: {
        women: toInt(caseFormInput.womenPob),
        men: toInt(caseFormInput.menPob),
        medical: toInt(caseFormInput.medicalPob),
        missing: toInt(caseFormInput.missingPob),
        drowned: toInt(caseFormInput.drownedPob),
        minors: toInt(caseFormInput.minorsPob),
        total: toInt(caseFormInput.totalPob)
      },
      freetext: caseFormInput.freetext,
      links: caseFormInput.links?.length ? caseFormInput.links.map(link => link['value']) : [],
      frontexInvolvement: caseFormInput.frontexInvolvement
        ? FrontextInvolvement[caseFormInput.frontexInvolvement]
        : null,
      outcome: caseFormInput.outcome ? CaseOutcome[caseFormInput.outcome] : null,
      status: caseFormInput.status ? CaseStatus[caseFormInput.status] : null,
      estimatedDepartureTime: departureTime,
      authoritiesAlerted: caseFormInput.authoritiesAlerted,
      authoritiesDetails: caseFormInput.authoritiesDetails
    }
  }

  caseObject.case.tags = caseFormInput.tags?.map(tag => ({ name: tag.value })) || []

  logger.trace(
    `Converted form input to case create input with values '${JSON.stringify(caseObject)}'`
  )

  return caseObject
}

/**
 * Converts contents of the case form to the data structure required by the UPDATE api
 * @param caseFormInput
 */
export const convertFormToUpdate = (caseFormInput: CaseFormInput): UpdateCaseMutationVariables => {
  const timestamp = dateToBackendString(
    combineDateTime(caseFormInput.creationDate!, caseFormInput.creationTime!)
  )

  const departureTime =
    caseFormInput.estimatedDepartureDate && caseFormInput.estimatedDepartureTime
      ? dateToBackendString(
          combineDateTime(
            caseFormInput.estimatedDepartureDate!,
            caseFormInput.estimatedDepartureTime!
          )
        )
      : null

  const caseObject: UpdateCaseMutationVariables = {
    case: {
      id: caseFormInput.id ?? '',
      caseNumber: caseFormInput.caseNumber!,
      timestamp: timestamp,
      boat: {
        type: toBoatType(caseFormInput.boatType!),
        color: toBoatColor(caseFormInput.boatColor!),
        engineStatus: caseFormInput.engineStatus ?? ''
      },
      peopleOnBoard: {
        women: toInt(caseFormInput.womenPob),
        men: toInt(caseFormInput.menPob),
        medical: toInt(caseFormInput.medicalPob),
        missing: toInt(caseFormInput.missingPob),
        drowned: toInt(caseFormInput.drownedPob),
        minors: toInt(caseFormInput.minorsPob),
        total: toInt(caseFormInput.totalPob)
      },
      freetext: caseFormInput.freetext,
      links: caseFormInput.links?.length ? caseFormInput.links.map(link => link['value']) : [],
      frontexInvolvement: caseFormInput.frontexInvolvement
        ? FrontextInvolvement[caseFormInput.frontexInvolvement]
        : null,
      outcome: caseFormInput.outcome ? CaseOutcome[caseFormInput.outcome] : null,
      status: caseFormInput.status ? CaseStatus[caseFormInput.status] : null,
      estimatedDepartureTime: departureTime,
      authoritiesAlerted: caseFormInput.authoritiesAlerted,
      authoritiesDetails: caseFormInput.authoritiesDetails
    }
  }

  caseObject.case.tags = caseFormInput.tags?.map(tag => ({ name: tag.value })) || []

  logger.trace(
    `Converted form input to case update input with values '${JSON.stringify(caseObject)}'`
  )

  return caseObject
}

/**
 * Converts the query result for a single case to the data structure expected by the case form.
 * @param caseFormInput
 */
export const convertCaseToFormInput = (caseObject: CasesQuery_cases_cases): CaseFormInput => {
  const formInput: CaseFormInput = {
    id: caseObject.id,
    caseNumber: caseObject.caseNumber,
    creationDate: backendStringToInputDateString(caseObject.timestamp!),
    creationTime: backendStringToInputTimeString(caseObject.timestamp!),
    freetext: caseObject.freetext,
    boatType: caseObject.boat?.type,
    boatColor: caseObject.boat?.color,
    engineStatus: caseObject.boat?.engineStatus,
    womenPob: caseObject.peopleOnBoard?.women,
    menPob: caseObject.peopleOnBoard?.men,
    minorsPob: caseObject.peopleOnBoard?.minors,
    drownedPob: caseObject.peopleOnBoard?.drowned,
    missingPob: caseObject.peopleOnBoard?.missing,
    medicalPob: caseObject.peopleOnBoard?.medical,
    totalPob: caseObject.peopleOnBoard?.total,
    links: caseObject.links?.length
      ? caseObject.links.map(link => ({
          value: link
        }))
      : [],
    tags: caseObject.tags?.length
      ? caseObject.tags?.map((tag: CasesQuery_cases_cases_tags | null) => ({
          value: tag?.name || null
        }))
      : [],
    estimatedDepartureTime: caseObject.estimatedDepartureTime,
    authoritiesAlerted: caseObject.authoritiesAlerted,
    authoritiesDetails: caseObject.authoritiesDetails,
    frontexInvolvement: caseObject.frontexInvolvement,
    outcome: caseObject.outcome,
    status: caseObject.status
  } as CaseFormInput

  logger.trace(`Converted case to form input with values '${JSON.stringify(formInput)}' `)

  return formInput
}

export const determineTotalPob = (caseObject: CasesQuery_cases_cases): number => {
  const pob = caseObject.peopleOnBoard
  if (!pob) return 0
  // It's valid to assume that all properties are not null if the object is poulated in the service response object
  return pob.total! !== 0 ? pob.total! : pob.women! + pob.men! + pob.minors!
}
