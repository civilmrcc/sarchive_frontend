import {
  combineDateTime,
  dateToBackendString,
  dateToInputTimeString,
  dateToInputDateString,
  backendStringToDisplayDateTimeString,
  backendStringToInputDateString,
  backendStringToInputTimeString
} from './DateTimeConverter'

describe('combineDateTime', () => {
  it('Combine UI date time input', () => {
    const dateString = '2000-01-01'
    const timeString = '11:28'
    expect(combineDateTime(dateString, timeString)).toEqual(new Date('2000-01-01 11:28:00'))
  })
})

describe('dateToBackendString', () => {
  it('Transform to expected date/time format for BE timestamps', () => {
    const date = new Date(2000, 0, 1, 11, 28, 0, 0)
    expect(dateToBackendString(date)).toEqual('2000-01-01 11:28:00')
  })
  it('Transform null to placeholder', () => {
    expect(dateToBackendString(null)).toEqual('-')
  })
})

describe('backendStringToDisplayDateTimeString', () => {
  it('Transform from the backends date/time format to the UI time format', () => {
    const backendString = '2000-01-01 11:28:00'
    expect(backendStringToDisplayDateTimeString(backendString)).toEqual('01.01.2000 11:28z')
  })
})

describe('backendStringToInputDateString', () => {
  it('should convert from the backend date/time format to the date format required by <input />', () => {
    const backendString = '2000-01-01 11:28:00'
    expect(backendStringToInputDateString(backendString)).toBe('2000-01-01')
  })
})

describe('backendStringToInputTimeString', () => {
  it('should convert from the backend date/time format to the time format required by <input />', () => {
    const backendString = '2000-01-01 11:28:00'
    expect(backendStringToInputTimeString(backendString)).toBe('11:28')
  })
})

describe('dateToInputTimeString', () => {
  it.each([
    [new Date(2000, 0, 1, 11, 28, 0, 0), '11:28'],
    [new Date(2000, 0, 1, 1, 8, 0, 0), '01:08']
  ])('should transform date to short time string: %s', (date, expected) => {
    expect(dateToInputTimeString(date)).toBe(expected)
  })
})

describe('dateToInputDateString', () => {
  it('should convert a date object to the date string used by then backend', () => {
    const date = new Date(2000, 0, 1, 11, 28, 0, 0)
    expect(dateToInputDateString(date)).toBe('2000-01-01')
  })
})
