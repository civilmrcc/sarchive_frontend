import { parseCoordinates as parseCoordinatesInternal } from 'coordinate-formats'
import {
  ValidationErrors as ValidationErrorsInternal,
  LatLon as LatLonInternal,
  ParsingResponse as ParsingResponseInternal
} from 'coordinate-formats/lib/types'

const COORDINATE_REGEX = /()(\d{1,2})(\d{1,2})(\d{1,2})()[/]()(\d{1,3})(\d{1,2})(\d{1,2})()/

/**
 * Parses coordinates input in format 'DDMMM/DDDMM'
 * @param coordinateInput
 */
export const parseCoordinates = (coordinateInput: string): ParsingResponse => {
  try {
    return parseCoordinatesInternal(coordinateInput, COORDINATE_REGEX)
  } catch (e) {
    return { errors: [e] } as ParsingResponse
  }
}

export declare type ValidationErrors = ValidationErrorsInternal
export declare type LatLon = LatLonInternal
export declare type ParsingResponse = ParsingResponseInternal
