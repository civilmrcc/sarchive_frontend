import { CasesQuery_cases_cases_positions } from '../../components/Cases/graphql/CasesQuery'
import { PositionInput } from '../../global-query-types'
import PositionFormInput from '../../models/PositionFormInput'
import { combineDateTimeToBackendString } from './DateTimeConverter'

export function positionFormInputToInput(positionFormInput: PositionFormInput): PositionInput {
  return {
    latitude: positionFormInput.latitude,
    longitude: positionFormInput.longitude,
    courseOverGround: parseFloat(positionFormInput.courseOverGround),
    speedOverGround: parseFloat(positionFormInput.speedOverGround),
    timestamp: combineDateTimeToBackendString(positionFormInput.date, positionFormInput.time)
  }
}

export function positionInputToPosition(
  positionInput: PositionInput,
  caseId: string,
  positionId?: string
): CasesQuery_cases_cases_positions {
  return {
    __typename: 'Position',
    id: positionId || '',
    caseId,
    courseOverGround: positionInput.courseOverGround || null,
    latitude: positionInput.latitude,
    longitude: positionInput.longitude,
    speedOverGround: positionInput.speedOverGround || null,
    timestamp: positionInput.timestamp || null
  }
}
