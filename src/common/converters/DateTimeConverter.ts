export function combineDateTime(dateString: string, timeString: string): Date | null {
  return timestampStringToDate(dateString + ' ' + timeString)
}

export function combineDateTimeToBackendString(dateString: string, timeString: string): string {
  const combinedDate = combineDateTime(dateString, timeString)
  return combinedDate ? dateToBackendString(combinedDate) : '-'
}

function padDigits(monthOrDate: number) {
  return monthOrDate.toString().padStart(2, '0')
}

export function dateToInputDateString(date: Date): string {
  return `${date.getFullYear()}-${padDigits(date.getMonth() + 1)}-${padDigits(date.getDate())}`
}

export function dateToBackendString(date: Date | null): string {
  if (!date) {
    return '-'
  }
  return `${dateToInputDateString(date)} ${padDigits(date.getHours())}:${padDigits(
    date.getMinutes()
  )}:${padDigits(date.getSeconds())}`
}

export function dateToInputTimeString(date: Date): string {
  return `${padDigits(date.getHours())}:${padDigits(date.getMinutes())}`
}

/**
 * Converts the backend date & time format to the display format
 * @param backendDateTimeString Backend timestamp string in YYYY-MM-DD hh:MM:ss format, example 2020-09-19 03:22:44
 */
export function backendStringToDisplayDateTimeString(
  backendDateTimeString: string | null | undefined
): string {
  if (!backendDateTimeString) {
    return '-'
  }
  const date = timestampStringToDate(backendDateTimeString)
  if (!date) {
    return '-'
  }
  return `${padDigits(date.getDate())}.${padDigits(
    date.getMonth() + 1
  )}.${date.getFullYear()} ${dateToInputTimeString(date)}z`
}

export function backendStringToInputDateString(backendDateTimeString: string): string {
  if (!backendDateTimeString) {
    return '-'
  }
  return backendDateTimeString.split(' ')[0]
}

export function backendStringToInputTimeString(backendDateTimeString: string): string {
  const date = timestampStringToDate(backendDateTimeString)
  return date ? dateToInputTimeString(date) : '-'
}

export function getCurrentUTCDate() {
  const now = new Date()
  return new Date(
    now.getUTCFullYear(),
    now.getUTCMonth(),
    now.getUTCDate(),
    now.getUTCHours(),
    now.getUTCMinutes(),
    now.getUTCSeconds()
  )
}

function timestampStringToDate(timestamp: string): Date | null {
  if (!timestamp) {
    return null
  }
  const dateComponents = timestamp.split(/[- :]/)
  return new Date(
    +dateComponents[0],
    +dateComponents[1] - 1,
    +dateComponents[2],
    +dateComponents[3],
    +dateComponents[4],
    0
  )
}
