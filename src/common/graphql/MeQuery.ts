/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MeQuery
// ====================================================

export interface MeQuery_me {
  __typename: "User";
  username: string;
}

export interface MeQuery {
  /**
   * Returns the currently logged in user.
   */
  me: MeQuery_me | null;
}
