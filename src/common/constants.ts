/**
 * Object defining all modal dialogs availble in the application.
 */
export const Modals = {
  login: 'loginModal',
  changePassword: 'changePasswordModal',
  addPosition: 'addPositionModal',
  editCase: 'editCaseModal'
}

/**
 * Object defining all events availble in the application.
 */
export const Events = {
  // authentication
  showLoginModal: 'showLoginModal',
  hideLoginModal: 'hideLoginModal',
  showPasswordChangeModal: 'showPasswordChangeModal',
  hidePasswordChangeModal: 'hidePasswordChangeModal',
  showAddPositionModal: 'showAddPositionModal',
  hideAddPositionModal: 'hideAddPositionModal',
  loggedIn: 'loggedIn',
  loggedOut: 'loggedOut',
  tokenExpired: 'tokenExpired',

  // generic
  error: 'error',
  successMessage: 'successMessage',

  // app events
  addEditCaseSuccess: 'addEditCaseSuccess',
  showEditCaseModal: 'showEditCaseModal',
  showAddCaseModal: 'showAddCaseModal',
  addPositionSuccess: 'addPositionSuccess',

  // csv export
  exportCsv: 'exportCsv',

  // control events
  input: 'input'
}

export const debounceMs = 200
