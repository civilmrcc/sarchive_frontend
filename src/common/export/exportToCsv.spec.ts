import { CasesQuery_cases_cases } from '../../components/Cases/graphql/CasesQuery'
import { BoatColor, BoatType } from '../../global-query-types'
import { arrayToCsvString } from './exportToCsv'

describe('exportToCsv', () => {
  const testData: CasesQuery_cases_cases[] = [
    {
      __typename: 'Case',
      id: '1',
      boat: {
        color: BoatColor.RED,
        type: BoatType.RUBBER,
        engineStatus: 'test',
        __typename: 'Boat'
      },
      caseNumber: 'test',
      freetext: 'test',
      links: ['test'],
      peopleOnBoard: {
        __typename: 'PeopleOnBoard',
        drowned: 0,
        medical: 0,
        men: 0,
        minors: 0,
        missing: 0,
        total: 0,
        women: 0
      },
      positions: [
        {
          __typename: 'Position',
          caseId: '1',
          id: 'p1',
          latitude: 47.11,
          longitude: 13.4,
          timestamp: '2020-11-01 11:59:59',
          courseOverGround: null,
          speedOverGround: null
        }
      ],
      timestamp: '2020-11-01 11:59:59',
      tags: [{ __typename: 'Tag', name: 'tag1' }],
      authoritiesAlerted: false,
      authoritiesDetails: null,
      estimatedDepartureTime: null,
      frontexInvolvement: null,
      outcome: null,
      status: null
    }
  ]

  it('arrayToCsvString - as expected', () => {
    const result = arrayToCsvString(testData)
    expect(result).toContain('test')
    expect(result).toContain('RUBBER')
    expect(result).toContain('2020-11-01 11:59:59')
    expect(result).toContain('tag1')
    expect(result).toContain('47.11')
  })
})
