/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-use-before-define */
import { filterGraphQlProperties } from '../helpers'

/**
 * Temporary implementation of CSV export. Will probably move to backend eventually.
 * @param array
 */
const exportToCsv = (array: any[]) => {
  const dataUri = 'data:text/csv;charset=utf-8,' + arrayToCsvString(array)
  const downloadLink = createDownloadLink(dataUri)
  downloadLink.click()
}

export const arrayToCsvString = (array: any[]): string => {
  const headers = getCsvColumnNames(array)
  const rows = array.map(element => elementToCsvRow(element))

  rows.splice(0, 0, headers)

  return rows.join('\n')
}

const elementToCsvRow = (element: any, separator = ';'): string => {
  const simpleObject = filterGraphQlProperties(element)

  return Object.values(simpleObject)
    .map(valueToString)
    .join(separator)
}

const valueToString = (value: any): string => {
  if (Array.isArray(value)) {
    return value.map(valueToString).join('|')
  }
  if (typeof value === 'object') {
    return elementToCsvRow(value, '|')
  }
  return JSON.stringify(value)
}

const createDownloadLink = (url: string): HTMLAnchorElement => {
  const encodedUri = encodeURI(url)
  const link = document.createElement('a')
  const timestamp = new Date().toISOString()

  link.setAttribute('href', encodedUri)
  link.setAttribute('download', `${timestamp}_sarchive_export.csv`)
  //   document.body.appendChild(link)

  return link
}

const getCsvColumnNames = (array: any[]): string => {
  if (!array?.length) {
    return ''
  }

  const firstObject = filterGraphQlProperties(array[0])
  return Object.keys(firstObject)
    .map((key: string) => key.toUpperCase())
    .join(';')
}

export default exportToCsv
