// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function filterGraphQlProperties(graphQlObject: any) {
  if (!graphQlObject) return {}
  const result = {}
  Object.keys(graphQlObject)
    .filter(propertyName => propertyName !== '__typename')
    .map(key => (result[key] = graphQlObject[key]))
  return result
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function transformToKeyValuePairs(obj: object): { key: string; value: any }[] {
  if (!obj) return []
  return Object.keys(obj).map(key => ({ key, value: obj[key] }))
}

export function copyPermalink(objectId: string) {
  navigator.clipboard.writeText(`${window.location.hostname}:${window.location.port}/${objectId}`)
}

export function openExternalUrl(url: string) {
  if (!url) {
    return
  }

  if (!url.startsWith('http://') && !url.startsWith('https://')) {
    url = `http://${url}`
  }

  window.open(url, '_blank')
}
