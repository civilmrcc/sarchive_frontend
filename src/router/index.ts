import Vue from 'vue'
import VueRouter from 'vue-router'
import Cases from '../components/Cases/Cases.vue'

Vue.use(VueRouter)

/**
 * Allow navigation to the overview as well as direct linking to a single case.
 */
const routes = [
  {
    path: '/',
    name: 'cases',
    component: Cases
  },
  {
    path: '/:id',
    component: Cases,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
