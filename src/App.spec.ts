import App from './App.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import serverBus from './common/serverBus'
import * as authentication from './common/authentication'
import { Events } from './common/constants'

jest.mock('./common/serverBus')

describe('App', () => {
  const render = (mocks: object = {}) => {
    // Use local vue instance to allow plugin mocking
    const localVue = createLocalVue()

    return shallowMount(App, {
      localVue,
      stubs: ['menu-bar', 'router-view', 'login', 'modals'],
      mocks: mocks
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })

  it('create registers serverBus', () => {
    const _ = render()
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const busOn = serverBus.$on as jest.Mock<any, any>
    expect(busOn).toHaveBeenCalled()
    expect(busOn).toHaveBeenCalledWith(Events.loggedIn, expect.anything())
    expect(busOn).toHaveBeenCalledWith(Events.loggedOut, expect.anything())
    expect(busOn).toHaveBeenCalledWith(Events.tokenExpired, expect.anything())
    expect(busOn).toHaveBeenCalledWith(Events.error, expect.anything())
  })

  it('logged in - dont show login', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.$data.showLoginModal).toBeFalsy()
  })
})
