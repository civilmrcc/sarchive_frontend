/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ErrorCodes } from "./../global-query-types";

// ====================================================
// GraphQL query operation: AppSettingsQuery
// ====================================================

export interface AppSettingsQuery_appSettings {
  __typename: "AppSettings";
  errorCodes: ErrorCodes | null;
}

export interface AppSettingsQuery {
  /**
   * Provides application settings to the frontend. Currently it only returns an `errorCodes` property
   * of the `ErrorCodes` enum. The payload ist always `null`. It is used by the UI framework to automatically
   * create an array of the error codes by the UI's GraphQL framework.
   */
  appSettings: (AppSettingsQuery_appSettings | null)[] | null;
}
