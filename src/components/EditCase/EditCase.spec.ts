import EditCase from './EditCase.vue'
import { shallowMount } from '@vue/test-utils'

function render() {
  return shallowMount(EditCase, { stubs: ['b-icon-backspace', 'error-alert'] })
}

describe('EditCase', () => {
  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })
})
