/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CategoriesQuery
// ====================================================

export interface CategoriesQuery_categories_tags {
  __typename: "Tag";
  name: string;
}

export interface CategoriesQuery_categories {
  __typename: "Category";
  id: string;
  name: string;
  tags: CategoriesQuery_categories_tags[] | null;
}

export interface CategoriesQuery {
  categories: (CategoriesQuery_categories | null)[] | null;
}
