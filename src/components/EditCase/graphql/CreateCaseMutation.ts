/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateCaseInput, BoatType, BoatColor, FrontextInvolvement, CaseOutcome, CaseStatus } from "./../../../global-query-types";

// ====================================================
// GraphQL mutation operation: CreateCaseMutation
// ====================================================

export interface CreateCaseMutation_createCase_boat {
  __typename: "Boat";
  type: BoatType | null;
  color: BoatColor | null;
  engineStatus: string | null;
}

export interface CreateCaseMutation_createCase_peopleOnBoard {
  __typename: "PeopleOnBoard";
  women: number | null;
  minors: number | null;
  men: number | null;
  medical: number | null;
  missing: number | null;
  drowned: number | null;
  total: number | null;
}

export interface CreateCaseMutation_createCase_tags {
  __typename: "Tag";
  name: string;
}

export interface CreateCaseMutation_createCase {
  __typename: "Case";
  id: string;
  caseNumber: string;
  freetext: string | null;
  links: (string | null)[] | null;
  timestamp: string | null;
  boat: CreateCaseMutation_createCase_boat | null;
  peopleOnBoard: CreateCaseMutation_createCase_peopleOnBoard | null;
  tags: (CreateCaseMutation_createCase_tags | null)[] | null;
  frontexInvolvement: FrontextInvolvement | null;
  outcome: CaseOutcome | null;
  status: CaseStatus | null;
  estimatedDepartureTime: string | null;
  authoritiesAlerted: boolean | null;
  authoritiesDetails: string | null;
}

export interface CreateCaseMutation {
  /**
   * Creates a new case.
   */
  createCase: CreateCaseMutation_createCase | null;
}

export interface CreateCaseMutationVariables {
  case: CreateCaseInput;
}
