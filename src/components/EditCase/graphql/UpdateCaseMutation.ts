/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateCaseInput, BoatType, BoatColor, FrontextInvolvement, CaseOutcome, CaseStatus } from "./../../../global-query-types";

// ====================================================
// GraphQL mutation operation: UpdateCaseMutation
// ====================================================

export interface UpdateCaseMutation_updateCase_boat {
  __typename: "Boat";
  type: BoatType | null;
  color: BoatColor | null;
  engineStatus: string | null;
}

export interface UpdateCaseMutation_updateCase_peopleOnBoard {
  __typename: "PeopleOnBoard";
  women: number | null;
  minors: number | null;
  men: number | null;
  medical: number | null;
  missing: number | null;
  drowned: number | null;
  total: number | null;
}

export interface UpdateCaseMutation_updateCase_tags {
  __typename: "Tag";
  name: string;
}

export interface UpdateCaseMutation_updateCase {
  __typename: "Case";
  id: string;
  caseNumber: string;
  freetext: string | null;
  links: (string | null)[] | null;
  timestamp: string | null;
  boat: UpdateCaseMutation_updateCase_boat | null;
  peopleOnBoard: UpdateCaseMutation_updateCase_peopleOnBoard | null;
  tags: (UpdateCaseMutation_updateCase_tags | null)[] | null;
  frontexInvolvement: FrontextInvolvement | null;
  outcome: CaseOutcome | null;
  status: CaseStatus | null;
  estimatedDepartureTime: string | null;
  authoritiesAlerted: boolean | null;
  authoritiesDetails: string | null;
}

export interface UpdateCaseMutation {
  /**
   * Updates an existing case. Calling this without an existing
   * id will result in an error. The API does not take care of generating
   * ids. That has to be handled by the caller.
   */
  updateCase: UpdateCaseMutation_updateCase | null;
}

export interface UpdateCaseMutationVariables {
  case: UpdateCaseInput;
}
