import gql from 'graphql-tag'
import { CreateCaseMutation, CreateCaseMutationVariables } from './graphql/CreateCaseMutation'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'

export const CREATE_CASE_MUTATION = gql`
  mutation CreateCaseMutation($case: CreateCaseInput!) {
    createCase(case: $case) {
      id
      caseNumber
      freetext
      links
      timestamp
      boat {
        type
        color
        engineStatus
      }
      peopleOnBoard {
        women
        minors
        men
        medical
        missing
        drowned
        total
      }
      tags {
        name
      }
      frontexInvolvement
      outcome
      status
      estimatedDepartureTime
      authoritiesAlerted
      authoritiesDetails
    }
  }
`

export async function createCase(
  apollo: DollarApollo<Vue>,
  caseObject: CreateCaseMutationVariables
) {
  await apollo.mutate<CreateCaseMutation, CreateCaseMutationVariables>({
    mutation: CREATE_CASE_MUTATION,
    variables: caseObject
  })
}
