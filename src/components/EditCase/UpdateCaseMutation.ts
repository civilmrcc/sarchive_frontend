import gql from 'graphql-tag'
import { MutationUpdaterFn } from 'apollo-client'
import { UpdateCaseMutation, UpdateCaseMutationVariables } from './graphql/UpdateCaseMutation'
import { CASES_QUERY } from '../Cases/CasesQuery'
import { CasesQuery, CasesQuery_cases_cases } from '../Cases/graphql/CasesQuery'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'

export const UPDATE_CASE_MUTATION = gql`
  mutation UpdateCaseMutation($case: UpdateCaseInput!) {
    updateCase(case: $case) {
      id
      caseNumber
      freetext
      links
      timestamp
      boat {
        type
        color
        engineStatus
      }
      peopleOnBoard {
        women
        minors
        men
        medical
        missing
        drowned
        total
      }
      tags {
        name
      }
      frontexInvolvement
      outcome
      status
      estimatedDepartureTime
      authoritiesAlerted
      authoritiesDetails
    }
  }
`

export async function updateCase(
  apollo: DollarApollo<Vue>,
  updatedCaseInput: UpdateCaseMutationVariables
) {
  await apollo.mutate<UpdateCaseMutation, UpdateCaseMutationVariables>({
    mutation: UPDATE_CASE_MUTATION,
    variables: updatedCaseInput
  })
}
