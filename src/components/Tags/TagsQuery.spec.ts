import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { refreshTags } from './TagsQuery'

describe('TagsQuery', () => {
  let apolloMock: Partial<DollarApollo<Vue>>

  beforeEach(() => {
    const queryMock = jest.fn().mockImplementation(() => {
      return Promise.resolve({
        data: {
          tags: [{ id: 'test-tag-1', name: 'test-1' }]
        }
      })
    })
    apolloMock = {
      query: queryMock
    }
  })
  it('refreshTags returns graphql result', async () => {
    const result = await refreshTags(apolloMock as DollarApollo<Vue>)
    expect(result).toHaveLength(1)
    expect(result[0]?.name).toEqual('test-1')
  })
})
