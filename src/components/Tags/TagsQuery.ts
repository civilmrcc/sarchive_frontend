import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import logger from '../../common/logger'
import { TagsQuery, TagsQuery_tags } from '../EditCase/graphql/TagsQuery'

export const TAGS_QUERY = gql`
  query TagsQuery {
    tags {
      id
      name
    }
  }
`

export interface TypeAheadTags {
  key: string
  value: string
}

export async function refreshTags(apollo: DollarApollo<Vue>): Promise<TagsQuery_tags[]> {
  const result = await apollo.query<TagsQuery>({ query: TAGS_QUERY })
  logger.trace(`Retrieved existing tags: '${JSON.stringify(result)}'`)
  return result?.data?.tags ? result.data.tags.map(tag => tag!) : []
}

export async function refreshTypeAhead(apollo: DollarApollo<Vue>): Promise<TypeAheadTags[]> {
  const tags = await refreshTags(apollo)
  return tags != null && tags.length > 0
    ? tags.map(
        tag =>
          ({
            key: tag.id,
            value: tag.name
          } as TypeAheadTags)
      )
    : []
}
