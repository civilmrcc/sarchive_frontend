import ChangePassword from './ChangePassword.vue'
import { shallowMount } from '@vue/test-utils'
import MockValidationObserver from '../../common/validation/MockValidationObserver.vue'

describe('ChangePassword', () => {
  const render = () => {
    return shallowMount(ChangePassword, {
      stubs: { 'validation-observer': MockValidationObserver },
      propsData: {
        static: true
      }
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })
})
