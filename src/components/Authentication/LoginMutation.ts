import gql from 'graphql-tag'
import { LoginMutation, LoginMutationVariables } from './graphql/LoginMutation'
import { FetchResult } from 'apollo-link'
import serverBus from '../../common/serverBus'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { Events } from '../../common/constants'
import { storeToken } from '../../common/authentication'

export const LOGIN_MUTATION = gql`
  mutation LoginMutation($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      token
      user {
        username
        isActive
        isPasswordInitial
        roles {
          name
          description
        }
      }
    }
  }
`

export async function triggerLogin(
  apollo: DollarApollo<Vue>,
  username: string,
  password: string
): Promise<void> {
  const { data, errors } = (await apollo.mutate<LoginMutation, LoginMutationVariables>({
    mutation: LOGIN_MUTATION,
    variables: { username: username, password: password }
  })) as FetchResult<LoginMutation>

  if (errors && errors.length) {
    throw Error('Authentication failed with error:' + errors[0])
  }

  if (!data?.login?.token) {
    throw Error('Authentication failed')
  }

  storeToken(data.login.token)

  serverBus.$emit(Events.loggedIn)
}
