import gql from 'graphql-tag'
import { FetchResult } from 'apollo-link'
import serverBus from '../../common/serverBus'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { Events } from '../../common/constants'
import {
  ChangePasswordMutation,
  ChangePasswordMutationVariables
} from './graphql/ChangePasswordMutation'
import logger from '../../common/logger'

export const CHANGE_PASSWORD_MUTATION = gql`
  mutation ChangePasswordMutation($oldPassword: String!, $newPassword: String!) {
    changePassword(oldPassword: $oldPassword, newPassword: $newPassword)
  }
`

export async function triggerPasswordChange(
  apollo: DollarApollo<Vue>,
  oldPassword: string,
  newPassword: string
): Promise<void> {
  const { data, errors } = (await apollo.mutate<
    ChangePasswordMutation,
    ChangePasswordMutationVariables
  >({
    mutation: CHANGE_PASSWORD_MUTATION,
    variables: {
      oldPassword: oldPassword,
      newPassword: newPassword
    }
  })) as FetchResult<ChangePasswordMutation>

  if (errors && errors.length) {
    serverBus.$emit(Events.loggedOut)
    throw Error('Password change failed with error:' + errors[0])
  }

  if (!data?.changePassword) {
    serverBus.$emit(Events.loggedOut)
    throw Error('Password change failed')
  }

  serverBus.$emit(Events.successMessage, 'Password changed successfully')
  logger.info('Password changed')

  serverBus.$emit(Events.hidePasswordChangeModal)
  serverBus.$emit(Events.showLoginModal)
}
