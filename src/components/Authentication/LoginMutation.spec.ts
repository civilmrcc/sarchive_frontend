import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { triggerLogin } from './LoginMutation'
import serverBus from '../../common/serverBus'
import * as authentication from '../../common/authentication'
import { Events } from '../../common/constants'

let apolloMock: Partial<DollarApollo<Vue>>
let storeTokenSpy: jest.SpyInstance
let serverBusEmitSpy: jest.SpyInstance

const mockToken = (testToken: string) => {
  apolloMock = {
    mutate: jest.fn().mockImplementation(() => {
      return {
        data: {
          login: {
            token: testToken,
            user: {
              username: 'test'
            }
          }
        }
      }
    })
  }
}

describe('LoginMutation', () => {
  beforeEach(() => {
    mockToken('valid-token')

    storeTokenSpy = jest.spyOn(authentication, 'storeToken')

    serverBusEmitSpy = jest.spyOn(serverBus, '$emit')
  })

  describe('triggerLogin', () => {
    it('throws for invalid token', () => {
      mockToken('')

      expect(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        triggerLogin(apolloMock as any, expect.any(String), expect.any(String))
      ).rejects.toThrowError('Authentication failed')
    })

    it('stores token on successful login', async () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await triggerLogin(apolloMock as any, expect.any(String), expect.any(String))

      expect(storeTokenSpy).toBeCalledWith('valid-token')
    })

    it('emits events on successful login', async () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await triggerLogin(apolloMock as any, expect.any(String), expect.any(String))

      expect(serverBusEmitSpy).toBeCalledWith(Events.loggedIn)
    })
  })
})
