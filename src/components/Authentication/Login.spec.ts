import Login from './Login.vue'
import { shallowMount } from '@vue/test-utils'
import MockValidationObserver from '../../common/validation/MockValidationObserver.vue'

describe('Login', () => {
  const render = () => {
    return shallowMount(Login, {
      stubs: { 'validation-observer': MockValidationObserver },
      propsData: {
        static: true
      }
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })

  // TODO test modal login's login functionality, with the current version of bootstrap vue, the button is not part of the markup, hence it is not possible to test the button
})
