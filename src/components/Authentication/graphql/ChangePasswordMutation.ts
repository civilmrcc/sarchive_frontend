/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ChangePasswordMutation
// ====================================================

export interface ChangePasswordMutation {
  /**
   * Change the users password.
   * If the current password is incorrect, an exception is thrown
   * If the current password is equals to the new password, an exception is thrown
   */
  changePassword: boolean | null;
}

export interface ChangePasswordMutationVariables {
  oldPassword: string;
  newPassword: string;
}
