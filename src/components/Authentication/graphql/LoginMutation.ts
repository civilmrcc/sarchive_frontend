/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: LoginMutation
// ====================================================

export interface LoginMutation_login_user_roles {
  __typename: "Role";
  name: string | null;
  description: string | null;
}

export interface LoginMutation_login_user {
  __typename: "User";
  username: string;
  isActive: boolean | null;
  isPasswordInitial: boolean | null;
  roles: (LoginMutation_login_user_roles | null)[] | null;
}

export interface LoginMutation_login {
  __typename: "LoginReturn";
  token: string;
  user: LoginMutation_login_user | null;
}

export interface LoginMutation {
  /**
   * The API is not public, so most queries and mutations
   * require you to send an authentication token, which has
   * to be retrieved by using the 'login' mutation.
   * If the isPasswordInitial attribute of a user is set to true,
   * a change of the password needs to be performed first. This needs
   * to be done by calling the changePassword mutation. Unless the
   * password is changed, all atter calls than changePassword will
   * lead to an exception
   */
  login: LoginMutation_login | null;
}

export interface LoginMutationVariables {
  username: string;
  password: string;
}
