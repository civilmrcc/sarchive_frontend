import Modals from './Modals.vue'
import { shallowMount } from '@vue/test-utils'

describe('Modals', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(Modals, { stubs: ['add-position'] })
    expect(wrapper.exists()).toBeTruthy()
  })
})
