import SuccessAlert from './SuccessAlert.vue'
import { shallowMount } from '@vue/test-utils'

describe('SuccessAlert', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(SuccessAlert)
    expect(wrapper.exists()).toBeTruthy()
  })
})
