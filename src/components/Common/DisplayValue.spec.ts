import DisplayValue from './DisplayValue.vue'
import { shallowMount } from '@vue/test-utils'

describe('DisplayValue', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(DisplayValue)
    expect(wrapper.exists()).toBeTruthy()
  })
})
