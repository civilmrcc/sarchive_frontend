import MenuBar from './MenuBar.vue'
import { shallowMount } from '@vue/test-utils'

describe('MenuBar', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(MenuBar, {
      stubs: [
        'b-icon-archive-fill',
        'b-icon-plus',
        'b-icon-search',
        'b-icon-power',
        'b-icon-download'
      ]
    })
    expect(wrapper.exists()).toBeTruthy()
  })
})
