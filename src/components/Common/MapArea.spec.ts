import MapArea from './MapArea.vue'
import { shallowMount } from '@vue/test-utils'

describe('MapArea', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(MapArea)
    expect(wrapper.exists()).toBeTruthy()
  })
})
