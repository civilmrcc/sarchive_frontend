import ErrorAlert from './ErrorAlert.vue'
import { shallowMount } from '@vue/test-utils'

describe('ErrorAlert', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(ErrorAlert)
    expect(wrapper.exists()).toBeTruthy()
  })
})
