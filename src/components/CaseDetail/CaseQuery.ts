import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { CaseQuery_case, CaseQuery } from './graphql/CaseQuery'
import { FetchResult } from 'apollo-link'
import logger from '../../common/logger'
import serverBus from '../../common/serverBus'
import { Events } from '../../common/constants'

export const CASE_QUERY = gql`
  query CaseQuery($id: ID!) {
    case(id: $id) {
      id
      caseNumber
      freetext
      links
      timestamp
      boat {
        type
        color
        engineStatus
      }
      peopleOnBoard {
        women
        minors
        men
        medical
        missing
        drowned
        total
      }
      positions {
        id
        caseId
        latitude
        longitude
        timestamp
      }
      tags {
        name
      }
    }
  }
`

export async function loadCase(
  apollo: DollarApollo<Vue>,
  caseNumber: string
): Promise<CaseQuery_case | undefined> {
  try {
    const result = (await apollo.query<CaseQuery>({
      query: CASE_QUERY,
      variables: { id: caseNumber }
    })) as FetchResult<CaseQuery>

    const data = result.data as CaseQuery
    logger.trace(`Loaded case with result '${JSON.stringify(data)}'`)

    return data.case!
  } catch (e) {
    logger.error(e)
    serverBus.$emit(Events.error, e)
  }
}
