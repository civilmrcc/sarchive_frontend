import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { CaseQuery } from './graphql/CaseQuery'
import { FetchResult } from 'apollo-link'
import { loadCase } from './CaseQuery'

describe('CaseQuery', () => {
  let apolloMock: Partial<DollarApollo<Vue>>

  beforeEach(() => {
    const queryMock = jest.fn().mockImplementation(() => {
      return Promise.resolve({
        data: {
          case: {
            id: 'test-case-1'
          }
        }
      } as FetchResult<Partial<CaseQuery>>)
    })
    apolloMock = {
      query: queryMock
    }
  })

  it('loadCase returns graphql result', async () => {
    const result = await loadCase(apolloMock as DollarApollo<Vue>, expect.anything())
    expect(result?.id).toEqual('test-case-1')
  })
})
