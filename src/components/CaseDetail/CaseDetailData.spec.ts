import CaseDetailData from './CaseDetailData.vue'
import { shallowMount } from '@vue/test-utils'
import {
  CaseQuery_case_peopleOnBoard,
  CaseQuery_case_boat,
  CaseQuery_case_tags
} from './graphql/CaseQuery'
import { BoatColor, BoatType } from '../../global-query-types'

const testCaseObject = {
  caseNumber: 'TEST',
  freetext: 'test',
  boat: {
    engineStatus: 'broken',
    color: BoatColor.RED,
    type: BoatType.RUBBER
  } as CaseQuery_case_boat,
  links: ['link1'],
  id: '1',
  positions: [],
  timestamp: '2020-01-01 12:12:12',
  peopleOnBoard: {
    drowned: 0,
    medical: 0,
    men: 1,
    women: 1,
    minors: 1,
    missing: 0,
    total: 1
  } as CaseQuery_case_peopleOnBoard,
  tags: [{ name: 'tag1' } as CaseQuery_case_tags, { name: 'tag2' } as CaseQuery_case_tags]
}

function render() {
  return shallowMount(CaseDetailData, {
    propsData: {
      caseObject: testCaseObject
    },
    stubs: ['b-icon-plus', 'b-icon-trash', 'b-icon-share', 'b-icon-pencil']
  })
}

describe('CaseDetailData', () => {
  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })

  it('renders tags', async () => {
    const wrapper = render()
    expect(wrapper.find('tag1')).toBeTruthy()
    expect(wrapper.find('tag2')).toBeTruthy()
  })
})
