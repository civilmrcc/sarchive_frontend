import CasesFilterPanel from './CasesFilterPanel.vue'
import { shallowMount } from '@vue/test-utils'
import { FilterInput } from '../../global-query-types'
import * as tagsQuery from '../Tags/TagsQuery'

function render() {
  return shallowMount(CasesFilterPanel, {
    propsData: {
      filter: {
        boatColors: [],
        boatTypes: [],
        caseDateFrom: null,
        caseDateTo: null,
        tags: [],
        text: null
      } as FilterInput
    },
    stubs: ['tags-input']
  })
}

describe('CasesFilterPanel', () => {
  beforeEach(() => {
    jest.spyOn(tagsQuery, 'refreshTypeAhead').mockImplementation(() => Promise.resolve([]))
  })

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })
})
