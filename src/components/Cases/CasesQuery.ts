import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { CasesQuery, CasesQuery_cases } from './graphql/CasesQuery'
import { FetchResult } from 'apollo-link'
import logger from '../../common/logger'
import { FilterInput, PaginationInput } from '../../global-query-types'

export const CASES_QUERY = gql`
  query CasesQuery($filter: FilterInput, $pagination: PaginationInput) {
    cases(filter: $filter, pagination: $pagination) {
      cases {
        id
        caseNumber
        freetext
        links
        timestamp
        boat {
          type
          color
          engineStatus
        }
        peopleOnBoard {
          women
          minors
          men
          medical
          missing
          drowned
          total
        }
        positions {
          id
          caseId
          latitude
          longitude
          timestamp
          speedOverGround
          courseOverGround
        }
        tags {
          name
        }
        frontexInvolvement
        outcome
        status
        estimatedDepartureTime
        authoritiesAlerted
        authoritiesDetails
      }
      responseMetadata {
        total
        page
        pages
        pageSize
        offset
      }
    }
  }
`

export async function loadCases(
  apollo: DollarApollo<Vue>,
  filter: FilterInput,
  pagination: PaginationInput
): Promise<CasesQuery_cases | null> {
  const result = (await apollo.query<CasesQuery>({
    query: CASES_QUERY,
    variables: { filter, pagination },
    // With the start of pagination and filtering,
    // the cache was disabled to avoid caching per parameter combination.
    fetchPolicy: 'no-cache'
  })) as FetchResult<CasesQuery>
  const data = result.data as CasesQuery

  logger.trace(`Refreshed cases with result '${JSON.stringify(data)}'`)
  return data.cases
}
