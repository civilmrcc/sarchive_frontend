import CaseListItem from './CaseListItem.vue'
import { shallowMount } from '@vue/test-utils'

function render() {
  return shallowMount(CaseListItem)
}

describe('CaseListItem', () => {
  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })
})
