import Cases from './Cases.vue'
import { shallowMount } from '@vue/test-utils'
import serverBus from '../../common/serverBus'
import * as authentication from '../../common/authentication'
import * as casesQuery from './CasesQuery'
import {
  CasesQuery_cases_cases_boat,
  CasesQuery_cases,
  CasesQuery_cases_cases,
  CasesQuery_cases_cases_peopleOnBoard
} from './graphql/CasesQuery'
import { BoatColor, BoatType } from '../../global-query-types'
import { Events } from '../../common/constants'

const testCaseObjects: CasesQuery_cases = {
  cases: [
    {
      __typename: 'Case',
      caseNumber: 'TEST',
      freetext: 'test',
      boat: {
        engineStatus: 'broken',
        color: BoatColor.RED,
        type: BoatType.RUBBER
      } as CasesQuery_cases_cases_boat,
      links: ['link1'],
      id: '1',
      positions: [],
      timestamp: '2020-01-01 12:12:12',
      peopleOnBoard: {
        drowned: 0,
        medical: 0,
        men: 1,
        women: 1,
        minors: 1,
        missing: 0
      } as CasesQuery_cases_cases_peopleOnBoard,
      tags: [],
      authoritiesAlerted: false,
      authoritiesDetails: null,
      estimatedDepartureTime: null,
      frontexInvolvement: null,
      outcome: null,
      status: null
    } as CasesQuery_cases_cases,
    {
      __typename: 'Case',
      caseNumber: 'TEST',
      freetext: 'test',
      boat: {
        engineStatus: 'broken',
        color: BoatColor.RED,
        type: BoatType.RUBBER
      } as CasesQuery_cases_cases_boat,
      links: ['link1'],
      id: '2',
      positions: [],
      timestamp: '2020-01-01 12:12:12',
      peopleOnBoard: {
        drowned: 0,
        medical: 0,
        men: 1,
        women: 1,
        minors: 1,
        missing: 0
      } as CasesQuery_cases_cases_peopleOnBoard,
      tags: []
    }
  ]
} as CasesQuery_cases

jest.mock('../../common/serverBus')

describe('Cases', () => {
  function render() {
    return shallowMount(Cases, {
      stubs: [
        'success-alert',
        'error-alert',
        'b-icon-download',
        'b-icon-plus',
        'b-icon-archive-fill',
        'b-icon-sort-down',
        'b-icon-funnel'
      ]
    })
  }

  it('Renders without error', () => {
    const wrapper = render()
    expect(wrapper.exists()).toBeTruthy()
  })

  it('create registers serverBus', async () => {
    render()
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const busOn = serverBus.$on as jest.Mock<any, any>
    expect(busOn).toHaveBeenCalled()
    expect(busOn).toHaveBeenCalledWith(Events.loggedIn, expect.anything())
    expect(busOn).toHaveBeenCalledWith(Events.loggedOut, expect.anything())
    expect(busOn).toHaveBeenCalledWith(Events.addEditCaseSuccess, expect.anything())
  })

  it('on mount loads cases', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)
    const loadCasesMock = jest.fn()
    jest.spyOn(casesQuery, 'loadCases').mockImplementation(loadCasesMock)

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(loadCasesMock).toBeCalledTimes(1)
  })

  it('loads cases - displays items', async () => {
    jest.spyOn(authentication, 'isAuthenticated').mockImplementation(() => true)
    jest.spyOn(casesQuery, 'loadCases').mockImplementation(() => Promise.resolve(testCaseObjects))

    const wrapper = render()
    await wrapper.vm.$nextTick()

    expect(wrapper.find('.caseListItem')).toBeTruthy()
  })
})
