import { loadCases } from './CasesQuery'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { FilterInput, PaginationInput } from '../../global-query-types'

describe('CasesQuery', () => {
  let apolloMock: Partial<DollarApollo<Vue>>

  beforeEach(() => {
    const queryMock = jest.fn().mockImplementation(async () => {
      return Promise.resolve({
        data: {
          cases: [{ id: 'test-case-1' }]
        }
      })
    })
    apolloMock = {
      query: queryMock
    }
  })

  it('loadCases returns graphql result', async () => {
    const cases = await loadCases(
      apolloMock as DollarApollo<Vue>,
      {} as FilterInput,
      {} as PaginationInput
    )
    expect(cases).toHaveLength(1)
  })
})
