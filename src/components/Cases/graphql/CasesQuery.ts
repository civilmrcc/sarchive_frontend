/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FilterInput, PaginationInput, BoatType, BoatColor, FrontextInvolvement, CaseOutcome, CaseStatus } from "./../../../global-query-types";

// ====================================================
// GraphQL query operation: CasesQuery
// ====================================================

export interface CasesQuery_cases_cases_boat {
  __typename: "Boat";
  type: BoatType | null;
  color: BoatColor | null;
  engineStatus: string | null;
}

export interface CasesQuery_cases_cases_peopleOnBoard {
  __typename: "PeopleOnBoard";
  women: number | null;
  minors: number | null;
  men: number | null;
  medical: number | null;
  missing: number | null;
  drowned: number | null;
  total: number | null;
}

export interface CasesQuery_cases_cases_positions {
  __typename: "Position";
  id: string;
  caseId: string;
  latitude: number;
  longitude: number;
  timestamp: string | null;
  speedOverGround: number | null;
  courseOverGround: number | null;
}

export interface CasesQuery_cases_cases_tags {
  __typename: "Tag";
  name: string;
}

export interface CasesQuery_cases_cases {
  __typename: "Case";
  id: string;
  caseNumber: string;
  freetext: string | null;
  links: (string | null)[] | null;
  timestamp: string | null;
  boat: CasesQuery_cases_cases_boat | null;
  peopleOnBoard: CasesQuery_cases_cases_peopleOnBoard | null;
  positions: (CasesQuery_cases_cases_positions | null)[] | null;
  tags: (CasesQuery_cases_cases_tags | null)[] | null;
  frontexInvolvement: FrontextInvolvement | null;
  outcome: CaseOutcome | null;
  status: CaseStatus | null;
  estimatedDepartureTime: string | null;
  authoritiesAlerted: boolean | null;
  authoritiesDetails: string | null;
}

export interface CasesQuery_cases_responseMetadata {
  __typename: "ResponseMetadata";
  total: number | null;
  page: number | null;
  pages: number | null;
  pageSize: number | null;
  offset: number | null;
}

export interface CasesQuery_cases {
  __typename: "CasesResponse";
  cases: (CasesQuery_cases_cases | null)[] | null;
  responseMetadata: CasesQuery_cases_responseMetadata | null;
}

export interface CasesQuery {
  cases: CasesQuery_cases | null;
}

export interface CasesQueryVariables {
  filter?: FilterInput | null;
  pagination?: PaginationInput | null;
}
