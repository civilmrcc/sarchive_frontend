import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import logger from '../../common/logger'
import {
  DeletePositionMutation,
  DeletePositionMutationVariables
} from '../Positions/graphql/DeletePositionMutation'
import { FetchResult } from 'apollo-link'
import serverBus from '../../common/serverBus'
import { Events } from '../../common/constants'

const DELETE_POSITION_MUTATION = gql`
  mutation DeletePositionMutation($id: ID!, $caseId: ID!) {
    deletePositionFromCase(id: $id, caseId: $caseId)
  }
`

export async function deletePosition(
  apollo: DollarApollo<Vue>,
  id: string,
  caseId: string
): Promise<boolean> {
  let success = false
  logger.trace(`Deleting position '${id}' of case '${caseId}'`)
  try {
    const result = (await apollo.mutate<DeletePositionMutation, DeletePositionMutationVariables>({
      mutation: DELETE_POSITION_MUTATION,
      variables: { id: id, caseId: caseId }
    })) as FetchResult<DeletePositionMutation>

    success = result.data != null && result.data!.deletePositionFromCase!
    logger.trace(`Deleted position '${id}' of case '${caseId}'. Success: '${success}'`)
  } catch (e) {
    logger.error(e)
    serverBus.$emit(Events.error, e)
  }
  return success
}
