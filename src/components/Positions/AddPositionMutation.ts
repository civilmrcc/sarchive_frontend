import gql from 'graphql-tag'
import { DollarApollo } from 'vue-apollo/types/vue-apollo'
import { PositionInput } from '../../global-query-types'
import { combineDateTimeToBackendString } from '../../common/converters/DateTimeConverter'
import logger from '../../common/logger'
import { AddPositionToCase, AddPositionToCaseVariables } from './graphql/AddPositionToCase'
import { MutationUpdaterFn } from 'apollo-client'
import { CASES_QUERY } from '../Cases/CasesQuery'
import { CasesQuery, CasesQuery_cases_cases_positions } from '../Cases/graphql/CasesQuery'
import PositionFormInput from '../../models/PositionFormInput'

export const ADD_POSITION_MUTATION = gql`
  mutation AddPositionToCase($caseId: ID!, $position: PositionInput!) {
    addPositionToCase(caseId: $caseId, position: $position) {
      id
      latitude
      longitude
      timestamp
      caseId
      speedOverGround
      courseOverGround
    }
  }
`

export async function addPositionToCase(
  apollo: DollarApollo<Vue>,
  caseId: string,
  positionInput: PositionInput
) {
  logger.trace(
    `Adding postion with data '${JSON.stringify(positionInput)}' to case with ID '${caseId}'`
  )

  await apollo.mutate<AddPositionToCase, AddPositionToCaseVariables>({
    mutation: ADD_POSITION_MUTATION,
    variables: {
      caseId: caseId,
      position: positionInput
    }
  })
}
