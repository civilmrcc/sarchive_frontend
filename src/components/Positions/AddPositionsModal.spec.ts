import AddPositionModal from './AddPositionModal.vue'
import { shallowMount } from '@vue/test-utils'
import * as addPositionMutation from './AddPositionMutation'

describe('AddPositionModal', () => {
  beforeEach(() => {
    jest.spyOn(addPositionMutation, 'addPositionToCase').mockImplementation(() => Promise.resolve())
  })

  it('Renders without error', () => {
    const wrapper = shallowMount(AddPositionModal, { stubs: ['input-field'] })
    expect(wrapper.exists()).toBeTruthy()
  })
})
