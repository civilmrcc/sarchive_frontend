import Positions from './Positions.vue'
import { shallowMount } from '@vue/test-utils'

describe('Positions', () => {
  it('Renders without error', () => {
    const wrapper = shallowMount(Positions, { stubs: ['b-icon-backspace'] })
    expect(wrapper.exists()).toBeTruthy()
  })
})
