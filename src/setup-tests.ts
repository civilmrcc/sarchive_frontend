import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import { ValidationObserver, ValidationProvider } from 'vee-validate'
import './common/validation/validations'

Vue.use(BootstrapVue)

// Mock dependencies we do not want to test
jest.mock('./common/logger')
jest.mock('apollo-client')
jest.mock('apollo-link')
jest.mock('apollo-link-http')
jest.mock('leaflet')
jest.mock('vue2-leaflet')
jest.mock('@voerro/vue-tagsinput', () => '<div>TAGS_INPUT</div>')

// Load dependencies required for rendering
Vue.component('validation-observer', ValidationObserver)
Vue.component('validation-provider', ValidationProvider)

const app = document.createElement('div')
app.setAttribute('id', 'app')
document.body.appendChild(app)
