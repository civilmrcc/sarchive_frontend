export default interface Position {
  longitude: number
  latitude: number
}
