export default interface CaseFormInput {
  id?: string
  caseNumber?: string
  creationDate?: string
  creationTime?: string
  womenPob?: number
  menPob?: number
  minorsPob?: number
  medicalPob?: number
  missingPob?: number
  drownedPob?: number
  totalPob?: number
  boatType?: string
  boatColor?: string
  engineStatus?: string
  links?: { value: string }[]
  tags?: { value: string }[]
  freetext?: string
  status?: string
  frontexInvolvement?: string
  outcome?: string
  estimatedDepartureDate?: string
  estimatedDepartureTime?: string
  authoritiesAlerted?: boolean
  authoritiesDetails?: string
}
