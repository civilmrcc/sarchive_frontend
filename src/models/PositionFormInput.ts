export default interface PositionFormInput {
  latitude: number
  longitude: number
  date: string
  time: string
  speedOverGround: string
  courseOverGround: string
}
