all: 
	docker pull registry.gitlab.com/civilmrcc/sarchive_frontend/master:latest
	docker pull registry.gitlab.com/civilmrcc/sarchive_backend/master:latest
	docker-compose up -d

down:
	docker-compose down

chrome-debug-mac:
	/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --remote-debugging-port=9666